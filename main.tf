module "user1webserver" {
  source           = "github.com/mounishareddy574/webserver.git"
  region           = "ap-northeast-1"
  key_name         = "Devops"
  ami              = "ami-0d52744d6551d851e"
  instance_type    = var.instance_type
  private_key_path = var.private_key_path

}